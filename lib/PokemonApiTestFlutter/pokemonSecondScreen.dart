import 'package:flutter/material.dart';

class SecondScreen extends StatefulWidget {
  @override
  State createState() {
    return _SecondScreen();
  }
}

class _SecondScreen extends State<SecondScreen> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        "SecondScreen",
      ),
    );
  }
}
