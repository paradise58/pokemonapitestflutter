import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:http/http.dart' as http;

class MainScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MainScreenState();
  }
}

class MainScreenState extends State<MainScreen> {
  var cachedPokemon = new List<Pokemon>();
  var offsetLoaded = new Map<int, bool>();
  var _pageSize;
  var hasConnection;

  @override
  void initState() {
    // начинаем загрузку первой порции покемонов
    hasConnection = true;
    _checkInternetConnection();
    _pageSize = 36;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _getBody();
  }

  _getBody() {
    if (_showLoadingDialog()) {
      if (!hasConnection) return _getErrorDialog();
      return _getProgressDialog();
    } else
      return _getGridView();
  }

  _showLoadingDialog() {
    return cachedPokemon.length < _pageSize;
  }

  _checkInternetConnection() async {
    try {
      await http
          .get(
              "https://pokeapi.co/api/v2/evolution-chain/?offset=${0}&limit=${1}")
          .then((bool) {
        setState(() {
          hasConnection = true;
          print('try $hasConnection');
          _getPokemon(0);
        });
      });
    } on Exception catch (e) {
      setState(() {
        hasConnection = false;
        print('catch $hasConnection');
      });
    }
  }

  _getProgressDialog() {
    return Center(child: CircularProgressIndicator());
  }

  _getErrorDialog() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Wrap(
          children: <Widget>[
            Text(
              'Нет подключения к интернету.',
              style: TextStyle(
                fontSize: 32.0,
                letterSpacing: 1,
              ),
              textAlign: TextAlign.center,
              softWrap: true,
            )
          ],
        ),
        InkWell(
          child: Text(
            "перезагрузить",
            style: TextStyle(color: Colors.blue[600], fontSize: 32.0),
          ),
          onTap: () {
            setState(() {
              hasConnection = true;
              _checkInternetConnection();
            });
          },
        )
      ],
    );
  }

  _getGridView() {
    var gridView = GridView.builder(
      padding: EdgeInsets.only(bottom: 8.0),
      itemCount: cachedPokemon.length,
      itemBuilder: (context, index) {
        Pokemon pokemon = cachedPokemon[index];
        //Когда остается 48 покемона за экраном, закружаем еще порцию из 48 покемонов
        _getPokemon(index + _pageSize);
        print('index = $index');
        return Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              16.0,
            ),
          ),
          margin: EdgeInsets.all(8.0),
          elevation: 12.0,
          child: pokemon.sprite == 'null'
              ? null
              : Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Expanded(
                      child: Image.network(
                        pokemon.sprite,
                        fit: BoxFit.fill,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        pokemon.name,
                        textAlign: TextAlign.end,
                      ),
                    ),
                  ],
                ),
        );
      },
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 160.0,
      ),
    );
    return gridView;
  }

  Pokemon _getPokemon(int index) {
// если покемонов больше, чем запрашиваемый индекс, то даем покемона,
// если нет, то запршиваем из сети порцию(страницу) покемонов
    Pokemon pokemon =
        cachedPokemon.length > index ? cachedPokemon[index] : null;
    if (pokemon == null) {
      int offset = index ~/ _pageSize * _pageSize;
      // если такая страница уже загружена, то ничего не делаем
      // offset постраничная загрузка
      if (!offsetLoaded.containsKey(offset)) {
        offsetLoaded.putIfAbsent(offset, () => true);
        _getPokemons(index, _pageSize)
            .then((List<Pokemon> pokemons) => _updatePokemons(index, pokemons));
      }
    }
    return pokemon;
  }

  Future<List<Pokemon>> _getPokemons(int offset, int limit) async {
    print('_getPokemons + offset = $offset + limit = $limit');
    List<Pokemon> pokeList = [];
    String jsonString = await _getPokemonJson(offset, limit);
//    print(jsonString);
    // получаем страницу покемонов в jsone и парсим в список объектов
    List pokemons = json.decode(jsonString) as List;
    pokemons.forEach((element) {
      Map pokemon = element as Map;
      var name = pokemon['name'].toString();
      var sprite = pokemon['sprite'];
      var chainId = int.parse(pokemon['chainId']);
      pokeList.add(Pokemon(name, sprite, chainId));
    });
    pokeList.sort((p1, p2) {
      return p1.chainId > p2.chainId ? 1 : -2;
    });
    return pokeList;
  }
//  получаем страницу покемонов в json
  Future<String> _getPokemonJson(int offset, int limit) async {
//    print('offset = $offset  limit = $limit');
    String jsonStr = '[';
    await Observable.fromFuture(http.get(
            "https://pokeapi.co/api/v2/evolution-chain/?offset=$offset&limit=$limit"))
        .flatMap((response) {
          Map resultsJson = json.decode(response.body) as Map;
          List evolutionChains = resultsJson['results'] as List;
//          print('results + $evolutionChains');
          return Observable.fromIterable(evolutionChains);
        })
        .flatMap((evolutionChain) {
          var url = evolutionChain['url'];
//          print('url + $url');
          return Observable.fromFuture(
              http.get(url).then((evolutionChain) async {
            var pokemonJson;
            Map evolutionChainJson = json.decode(evolutionChain.body) as Map;
            var name = evolutionChainJson['chain']['species']['name'];
//            print('name + $name');
            var chainId = evolutionChainJson['id'];
            await http
                .get('https://pokeapi.co/api/v2/pokemon-form/$name')
                .then((formResponse) {
              Map form = json.decode(formResponse.body) as Map;
              var sprite = form['sprites']['front_default'];
//              print('sprite + $sprite');
              pokemonJson =
                  '{"chainId":"$chainId","name":"$name","sprite":"$sprite"},';
            });
            return pokemonJson;
          })).onErrorResumeNext(Observable.just(''));
        })
        .onErrorResumeNext(Observable.just(''))
        .listen((pokemonJson) {
          jsonStr += pokemonJson;
        })
        .asFuture();
    var str = jsonStr.substring(0, jsonStr.length - 1);
    str += ']';
//    print('jsont str  = $str');
    return str;
  }
// notifyDataSetChanged()
  void _updatePokemons(int offset, List<Pokemon> pokemons) {
    // типа проверка существует ли сейчас текущий виджет, в данном случае экран с покемонами
    // чтобы при переключении экрана, не вызывать метод setState(),
    // без него приводик к ошибке, но приложения не крашит
    if(mounted)
    setState(() {
      for (int i = 0; i < pokemons.length; i++) {
        if (pokemons[i].sprite != 'null') cachedPokemon.add(pokemons[i]);
      }
      print('$offset');
    });
  }
}

class Pokemon {
  var name;
  var sprite;
  var chainId;

  Pokemon(this.name, this.sprite, this.chainId);
}
