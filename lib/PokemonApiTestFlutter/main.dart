import 'package:PokemonApiTestFlutter/PokemonApiTestFlutter/pokemonRx.dart';
import 'package:PokemonApiTestFlutter/PokemonApiTestFlutter/pokemonSecondScreen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State createState() {
    return _MyApp();
  }
}

class _MyApp extends State<MyApp> with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Title",
      routes: {
        '/main': (BuildContext context) => MainScreen(),
        '/second': (BuildContext context) => SecondScreen()
      },
      home: Scaffold(
        //нестред скрол для управления скрола AppBar
        body: NestedScrollView(
          controller: ScrollController(),
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              //Прокачанные AppBar
              SliverAppBar(
                title: Text('SilverAppBar'),
                backgroundColor: Colors.red[800],
                pinned: true,
                floating: true,
                snap: true,
                forceElevated: innerBoxIsScrolled,
                bottom: ColoredTabBar(
                  Colors.black,
                  TabBar(
                    indicatorColor: Colors.red,
                    labelColor: Colors.red[800],
                    unselectedLabelColor: Colors.red[800],
                    tabs: <Tab>[
                      Tab(icon: Icon(Icons.cloud_download), text: 'Network'),
                      Tab(icon: Icon(Icons.folder), text: 'Local')
                    ],
                    controller: _tabController,
                  ),
                ),
              ),
            ];
          },
          body: TabBarView(
            children: <Widget>[
              MainScreen(),
              SecondScreen(),
            ],
            controller: _tabController,
          ),
        ),
      ),
    );
  }
}

//Кастомный TabBat с нужным цветом.
class ColoredTabBar extends Container implements PreferredSizeWidget {
  ColoredTabBar(this.color, this.tabBar);

  final Color color;
  final TabBar tabBar;

  @override
  Widget build(BuildContext context) => Container(
        color: color,
        child: tabBar,
      );

  @override
  Size get preferredSize => tabBar.preferredSize;
}
